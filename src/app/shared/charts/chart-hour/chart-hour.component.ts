import { Component, Input, OnChanges } from '@angular/core';

import { Consumo, DadosEstacoesHidrologia, DadosHidrologia } from '../../../models/report-daily-point.model';

@Component({
  selector: 'vtl-chart-hour',
  templateUrl: './chart-hour.component.html',
  styleUrls: ['./chart-hour.component.css']
})
export class ChartHourComponent implements OnChanges {

  @Input() chartType: string;
  @Input() dataChart: Consumo[] = [];
  @Input() dataChartHidro: DadosHidrologia[] = [];
  @Input() dateChart: string;
  @Input() label1: string;
  @Input() label2: string;

  dataDemandGenaration: Object = {};
  dataHidroSeasonsList: Object = {};
  options: Object = {};
  requestContract: number = 0;
  type: string = '';

  constructor() { }

  ngOnChanges() {
    let hour = this.dataChart.map((res: Consumo) => res.hora);
    let value1 = this.dataChart.map((res: Consumo) => res.valor1);
    let value2 = this.dataChart.map((res: Consumo) => res.valor2);
    this.requestContract = parseInt(value2[0]);

    this.type = 'line';
    this.dataDemandGenaration = {
      labels: hour,
      datasets: [
        {
          data: value1,
          label: this.label1 ? this.label1 : '',
          backgroundColor: 'rgba(255, 255, 255, 0.99)',
          borderColor: this.label1 ? 'rgba(30, 169, 252, 0.99)' : 'rgba(255, 255, 255, 0.99)',
          fill: false
        },
        {
          //#TODO: remover label se for undefined
          data: value2,
          label: this.label2 ? this.label2 : '',
          backgroundColor: 'rgba(255, 255, 255, 0.99)',
          borderColor: this.label2 ? 'rgba(244, 125, 32, 0.88)' : 'rgba(255, 255, 255, 0.99)',
          fill: false
        }
      ]
    };
    this.options = {
      responsive: true,
      title: {
        display: true,
        text: `Geração do dia ${this.dateChart}`,
      },
      maintainAspectRatio: false,
    };

    //HIDRO    
    let hourHidro = this.dataChartHidro.map((res: DadosHidrologia) => res.created);
    let value1Hidro = this.dataChartHidro.map((res: DadosHidrologia) => res.stage);
    let value2Hidro = this.dataChartHidro.map((res: DadosHidrologia) => res.flowConsistency);
    this.dataHidroSeasonsList = {
      labels: hourHidro,
      datasets: [
        {
          data: value1Hidro,
          label: this.label1 ? this.label1 : '',
          backgroundColor: 'rgba(255, 255, 255, 0.99)',
          borderColor: this.label1 ? 'rgba(30, 169, 252, 0.99)' : 'rgba(255, 255, 255, 0.99)',
          fill: false
        },
        {
          //#TODO: remover label se for undefined
          data: value2Hidro,
          label: this.label2 ? this.label2 : '',
          backgroundColor: 'rgba(255, 255, 255, 0.99)',
          borderColor: this.label2 ? 'rgba(244, 125, 32, 0.88)' : 'rgba(255, 255, 255, 0.99)',
          fill: false
        }
      ]
    };
  }
}
