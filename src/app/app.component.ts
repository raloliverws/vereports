import { Component } from '@angular/core';

@Component({
  selector: 'vtl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'vereports';
}
