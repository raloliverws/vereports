import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FooterComponent } from './footer/footer.component';
import { CardComponent } from './card/card.component';
import { DatatableComponent } from './datatable/datatable.component';
import { ChartsModule } from './charts/charts.module';
import { PanelComponent } from './panel/panel.component';
import { CardToggleDirective } from './panel/panel-toggle.directive';
import { CardFullComponent } from './card-full/card-full.component';
import { ResumeTableComponent } from './resume-table/resume-table.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    NgxDatatableModule,
    ChartsModule,
    NgbModule.forRoot()
  ],
  declarations: [
    FooterComponent,
    CardComponent,
    DatatableComponent,
    PanelComponent,
    CardToggleDirective,
    CardFullComponent,
    ResumeTableComponent
  ],
  exports: [
    NgbModule,
    FooterComponent,
    CardComponent,
    DatatableComponent,
    ChartsModule,
    PanelComponent,
    CardToggleDirective,
    CardFullComponent,
    ResumeTableComponent
  ]
})
export class SharedModule { }
