import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'vtl-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;

  columns = [
    { prop: 'lastdaygenmwh', name: 'Energia Ativa' },
    { prop: 'lastdaygenmhm', name: 'Energia Reativa' },
    { prop: 'lastdaycosmwh', name: 'Demanda Max' },
    { prop: 'lastdaycosmhm', name: 'Demanda Contr. P' },
    { prop: 'lastdaydempmax', name: 'Demanda Contr. FP' },
    { prop: 'lastdaydemfpmax', name: 'Fator Carga P' },
    { prop: 'lastdaydemfpmax', name: 'Fator Carga FP' }
  ];

  rows = [];

  rowsFilter = [];
  tempFilter = [];

  constructor() { }

  ngOnInit() {
    // this.fetchFilterData((data) => {
    //   this.tempFilter = [...data];
    //   this.rowsFilter = data;
    // });
  }

  fetchFilterData(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/mockup.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function (d) {
      return d.lastdaygenmwh.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
  }

}
