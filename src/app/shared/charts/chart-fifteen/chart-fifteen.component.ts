import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'vtl-chart-fifteen',
  templateUrl: './chart-fifteen.component.html',
  styleUrls: ['./chart-fifteen.component.css']
})
export class ChartFifteenComponent implements OnInit {

  type1 = 'bar';
  data1 = {
    labels: ['08:15', '08:30', '08:45', '9:00', '9:15', '9:30', '9:45', '10:00', '10:15', '10:30', '10:45', '11:00'],
    datasets: [{
      label: 'Demanda Máxima Ponta',
      backgroundColor: [
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)',
        'rgba(244, 125, 32, 0.99)'
      ],
      hoverBackgroundColor: [
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)',
        'rgba(244, 125, 32, 0.88)'
      ],
      data: [6500, 5900, 8000, 8100, 5600, 5500, 5000, 4500, 7000, 3400, 5000, 2200],
    }, {
      label: 'Demanda Máxima Fora Ponta',
      backgroundColor: [
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)',
        'rgba(30, 169, 252, 0.99)'
      ],
      hoverBackgroundColor: [
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)',
        'rgba(30, 169, 252, 0.88)'
      ],
      data: [6000, 6900, 8500, 9100, 5800, 5000, 4500, 4500, 8900, 6700, 3500, 5800],
    }]
  };
  options = {
    responsive: true,
    maintainAspectRatio: false,
    barValueSpacing: 20
  };

  constructor() { }

  ngOnInit() {
  }

}
