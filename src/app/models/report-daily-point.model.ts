export class ReportDailyPointModel {
    nomePonto: string;
    data: string;
    concessionaria: string;
    medidores: Medidores;
    consumoAtivoTotal: string;
    consumoAtivoPonta: string;
    consumoAtivoFora: string;
    geracaoAtivaTotal: string;
    geracaoAtivaPonta: string;
    geracaoAtivaFora: string;
    consumoReativo: string;
    geracaoReativa: string;
    demandaMaximaConsumoPonta: string;
    demandaMaximaConsumoFora: DemandaContratadaConsumoFora;
    demandaMaximaGeracaoPonta: string;
    demandaMaximaGeracaoFora: string;
    demandaContratadaConsumoPonta: DemandaContratadaConsumoFora;
    demandaContratadaConsumoFora: DemandaContratadaConsumoFora;
    demandaContratadaGeracaoPonta: DemandaContratadaConsumoFora;
    demandaContratadaGeracaoFora: DemandaContratadaConsumoFora;
    fatorCargaConsumoPonta: string;
    fatorCargaConsumoFora: string;
    fatorCargaGeracaoPonta: string;
    fatorCargaGeracaoFora: string;
    uferPonta: string;
    uferFora: string;
    dmcrPonta: string;
    dmcrFora: string;
    garantiaFisica: string;
    consumos: Consumo[];
    geracoes: Consumo[];
    demandasConsumo: Consumo[];
    demandasGeracao: Consumo[];
    dadosEstacoesHidrologia: DadosEstacoesHidrologia[];
}

export interface Consumo {
    hora: string;
    postoHorario: PostoHorario;
    valor1: string;
    valor2: DemandaContratadaConsumoFora | null;
}

export enum PostoHorario {
    Fora = "fora",
    Ponta = "ponta",
}

export enum DemandaContratadaConsumoFora {
    Null = "null",
    The50000 = "5000.0",
}

export interface DadosEstacoesHidrologia {
    nomeEstacao: string;
    fluviometro: string;
    pluviometro: string;
    limnimetro: string;
    fluvcode: string;
    pluvcode: string;
    chuvaAcumulada: string;
    nivelMinimo: string;
    nivelMaximo: string;
    nivelMedio: string;
    vazaoMinima: string;
    vazaoMaxima: string;
    vazaoMedia: string;
    dadosHidrologia: DadosHidrologia[];
    constantes: any[];
}

export interface Medidores {
    sitesMeters: { [key: string]: { [key: string]: null | string } };
}

export interface DadosHidrologia {
    id: string,
    created: string,
    siteHydroId: string,
    mailTime: string,
    time: string,
    rainfall: string,
    stage: string,
    flow: string,
    flowConsistency: string,
    mailQuantity: string,
    a1: null,
    a2: null,
    a3: null,
    a4: null,
    a5: null,
    a6: null,
    a7: null,
    a8: null,
    d1: null,
    d2: null,
    d3: null,
    d4: null,
    satelliteMessageId: string
}
