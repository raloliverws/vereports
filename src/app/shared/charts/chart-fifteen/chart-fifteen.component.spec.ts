import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartFifteenComponent } from './chart-fifteen.component';

describe('ChartFifteenComponent', () => {
  let component: ChartFifteenComponent;
  let fixture: ComponentFixture<ChartFifteenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartFifteenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartFifteenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
