import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

import { ReportDailyPointModel } from '../models/report-daily-point.model';

@Injectable({
  providedIn: 'root'
})
export class ReportDailyPointService {

  private token = 'M0NCME1KUkw5TENQUkMxNDk0NDQzNDdIMlU=';

  protected httpOptionsAuth = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.token
    })
  };

  apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  getReportDailyPoint(site: number, date: string): Observable<ReportDailyPointModel> {
    return this.http.get<ReportDailyPointModel>(`${this.apiURL}/relatorios/dadosDiarioPonto?site_id=${site}&data=${date}`, this.httpOptionsAuth);
  }
}
