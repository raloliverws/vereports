import { Component, OnInit, Input } from '@angular/core';

import { DadosEstacoesHidrologia } from '../../models/report-daily-point.model';

@Component({
  selector: 'vtl-card-full',
  templateUrl: './card-full.component.html',
  styleUrls: ['./card-full.component.css']
})
export class CardFullComponent implements OnInit {

  @Input() hidroSeasonsList: DadosEstacoesHidrologia[];

  constructor() { }

  ngOnInit() {
  }

}
