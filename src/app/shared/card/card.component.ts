import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'vtl-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() status: string = 'success';
  @Input() check: boolean = true;
  @Input() value: number;
  @Input() measure: string;



  constructor() { }

  ngOnInit() {
  }

}
