import { Component, OnInit, Input } from '@angular/core';
import { ReportDailyPointModel } from 'src/app/models/report-daily-point.model';

@Component({
  selector: 'vtl-resume-table',
  templateUrl: './resume-table.component.html',
  styleUrls: ['./resume-table.component.css']
})
export class ResumeTableComponent implements OnInit {

  @Input() reportDailyPointData: ReportDailyPointModel = null;

  constructor() { }

  ngOnInit() {
  }

}
