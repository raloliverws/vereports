import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { SharedModule } from '../shared/shared.module';
import { IndexComponent } from './index.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    ScrollToModule.forRoot()
  ],
  declarations: [
    IndexComponent
  ]
})
export class IndexModule { }
