import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import { ReportDailyPointModel, Consumo, DadosEstacoesHidrologia } from '../models/report-daily-point.model';
import { ReportDailyPointService } from '../services/report-daily-point.service';

@Component({
  selector: 'vtl-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  reportDailyPointData: ReportDailyPointModel;
  reportDailyPointDemandGen: Consumo[];
  reportDailyPointGeneration: Consumo[];
  reportDailyHidroSeasonsList: DadosEstacoesHidrologia[];

  site: any = 1;
  date: any = new Date().toJSON().slice(0, 10).replace(/-/g, '-');

  constructor(
    private route: ActivatedRoute,
    private reportDailyPointService: ReportDailyPointService
  ) { }

  ngOnInit() {
    //#TODO: tratar erro se a data ou o site forem inválidos
    this.site = this.route.snapshot.queryParamMap.get('site_id') || this.site;
    this.date = this.route.snapshot.queryParamMap.get('data') || this.date;

    this.getReportDailyPoint(this.site, this.date);
  }

  getReportDailyPoint(site, date) {
    this.reportDailyPointService.getReportDailyPoint(site, date).subscribe(
      (res: ReportDailyPointModel) => {
        this.reportDailyPointData = res;
        this.reportDailyPointDemandGen = res.demandasGeracao;
        this.reportDailyPointGeneration = res.geracoes;
        this.reportDailyHidroSeasonsList = res.dadosEstacoesHidrologia;
      }
    )
  }

  downloadPDF() {
    html2canvas(document.getElementById('reportDailyPointDemandGen')).then((canvas) => {
      let image = canvas.toDataURL("image/png");
      let file = new jsPDF();
      file.addImage(image, 'JPEG', 0, 0);
      file.save('digest-231118-econometer.pdf');
    });
  }
}
