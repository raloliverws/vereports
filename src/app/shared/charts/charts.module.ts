import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ChartModule} from 'angular2-chartjs';

import { ChartHourComponent } from './chart-hour/chart-hour.component';
import { ChartFifteenComponent } from './chart-fifteen/chart-fifteen.component';

@NgModule({
  imports: [
    CommonModule,
    ChartModule
  ],
  declarations: [
    ChartHourComponent,
    ChartFifteenComponent
  ],
  exports: [
    ChartHourComponent,
    ChartFifteenComponent
  ]
})
export class ChartsModule { }
