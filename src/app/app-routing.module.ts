import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { IndexComponent } from "./index/index.component";

export const VtRoutes: Routes = [
    {
        path: "",
        component: IndexComponent,
        data: {
            title: "Digest | Emeter"
        }
    },
    {
        path: '**',
        redirectTo: '/'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(VtRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }